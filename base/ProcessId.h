/**
* Created by Sergey O. Boyko on 08.07.18.
*/

#pragma once

namespace core::base {

/**
* Get current process id (pid)
* @return current pid
*/
unsigned int GetCurrentProcessId();

}