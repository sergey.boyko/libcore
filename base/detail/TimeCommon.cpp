/**
* Created by Sergey O. Boyko on 18.06.18.
*/

#include <sys/time.h>
#include <sstream>

#include "base/Formatter.h"
#include "TimeCommon.h"

namespace core::base::detail {

std::string GetDateAsString(std::time_t current_time) {
	struct tm tstruct {};
	tstruct = *localtime(&current_time);

	std::string month = std::to_string(tstruct.tm_mon + 1);
	std::string day = std::to_string(tstruct.tm_mday);
	std::string year = std::to_string(tstruct.tm_year + 1900);

	if(stoi(day) < 10) {
		day = "0" + day;
	}

	if(stoi(month) < 10) {
		month = "0" + month;
	}

	return year + "-" + month + "-" + day;
}


std::string GetCurrentTimeAsString(std::time_t current_time) {
	timeval time_val {};
	gettimeofday(&time_val, nullptr);
	auto milli = time_val.tv_usec / 1000;

	struct tm tstruct {};
	tstruct = *localtime(&current_time);

	return Format("%:%:%:%", tstruct.tm_hour, tstruct.tm_min, tstruct.tm_sec, milli);
}

}