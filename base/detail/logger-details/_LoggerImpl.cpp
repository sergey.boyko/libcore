/**
* Created by Sergey O. Boyko on 03.06.18.
*/

#include "_LoggerImpl.h"
#include "_LoggerOptions.h"

#include "base/Checking.h"

namespace core::base::detail {

_LoggerOptions _LoggerImpl::_InitializationOptions;

std::unique_ptr<_LoggerImpl> _LoggerImpl::_Instance;


_LoggerImpl::_LoggerImpl():
		_LoggerOptions(_InitializationOptions) {
	if(m_use_log_file && !openFileStream()) {
		m_is_initialization_error = true;
	}
}


_LoggerImpl::~_LoggerImpl() {
	if(m_log_fstream) {
		m_log_fstream->close();
	}
}


bool _LoggerImpl::InitLogger() {
	_Instance.reset(new _LoggerImpl());
	return !_Instance->m_is_initialization_error;
}


std::string _LoggerImpl::GetLastError() {
	if(IsInitialized()) {
		return _Instance->m_initialization_error;
	}

	return "";
}


bool _LoggerImpl::openFileStream() {
	auto mode = std::ios::app;
	if(m_clear_logs) {
		mode = std::ios::out;
	}

	if(!std::experimental::filesystem::exists(m_log_path)) {
		m_initialization_error = Format(R"('%' does not exist)", m_log_path.string());
		return false;
	}

	if(!std::experimental::filesystem::is_directory(m_log_path)) {
		m_initialization_error = Format(R"('%' is not a directory)", m_log_path.string());
		return false;
	}

	auto file_path = m_log_path / generateFileName();
	m_log_fstream = std::make_optional<std::ofstream>(file_path, mode);
	return true;
}


std::unique_ptr<_LoggerImpl> &_LoggerImpl::GetInstance() {
	if(_Instance) {
		return _Instance;
	}

	throw std::runtime_error("The logger is not initialized");
}


bool _LoggerImpl::IsInitialized() {
	if(_Instance) {
		return true;
	}

	return false;
}


void _LoggerImpl::UseLogFile(const std::experimental::filesystem::path &log_path) {
	_InitializationOptions.m_use_log_file = true;
	_InitializationOptions.m_log_path = log_path;
}


void _LoggerImpl::SetModuleName(std::string_view name) {
	if(name.empty()) {
		throw std::runtime_error("Module name can not be empty");
	}

	_InitializationOptions.m_module_name = name;
}


void _LoggerImpl::ClearLogs() noexcept {
	_InitializationOptions.m_clear_logs = true;
}


void _LoggerImpl::UsePidInMessageHeader() noexcept {
	_InitializationOptions.m_use_pid_in_message_headers = true;
}


void _LoggerImpl::UsePidInLogFileName() noexcept {
	_InitializationOptions.m_use_pid_in_file_name = true;
}


void _LoggerImpl::ClearOptions() {
	_InitializationOptions = {};
}


void _LoggerImpl::UseConsoleOutput() noexcept{
	_InitializationOptions.m_use_console_output = true;
}


void _LoggerImpl::SetLevel(unsigned int level) {
	if(level != LoggerLevel::None &&
		 level != LoggerLevel::Error &&
		 level != LoggerLevel::Info &&
		 level != LoggerLevel::Debug) {
		throw std::runtime_error("Unknown Logger level");
	}

	_InitializationOptions.m_logger_level = level;
}


void _LoggerImpl::LogEndl() {
	if(m_use_console_output) {
		std::cout << std::endl;
	}

	if(m_log_fstream) {
		*m_log_fstream << std::endl;
	}
}


std::string _LoggerImpl::generateFileName() {
	std::stringstream file_name_stream;
	file_name_stream << m_module_name;

	if(m_use_pid_in_file_name) {
		file_name_stream << Format("[%]", GetCurrentProcessId());
	}

	file_name_stream << Format(".%.log", GetDateAsString(time(nullptr)));
	return file_name_stream.str();
}


std::optional<std::lock_guard<std::mutex>> _LoggerImpl::getMutexLock() {
#ifdef CORE_MULTITHREADED
	return std::make_optional<std::lock_guard<std::mutex>>(m_mutex);
#endif
	return std::optional<std::lock_guard<std::mutex>>();
}


std::string _LoggerImpl::getLogTypeAsString(unsigned int log_type) {
	std::string str_log_type;
	switch(log_type) {
		case LoggerLevel::Debug: {
			return "DEBUG";
		}
		case LoggerLevel::Info: {
			return "INFO";
		}
		case LoggerLevel::Error: {
			return "ERROR";
		}
		default: {
			THROWEX_D(R"(log_type must be 'DEBUG' or 'INFO' or 'ERROR')");
		}
	}
}

}