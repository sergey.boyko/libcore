/**
* Created by Sergey O. Boyko on 03.06.18.
*/

#pragma once

#include <experimental/filesystem>

#include "base/LoggerLevel.h"

namespace core::base::detail {
struct _LoggerOptions {
	/**
	* Path to log directory.
	*/
	std::experimental::filesystem::path m_log_path;

	/**
	* The name of the module that is initializes the Logger.
	*/
	std::string m_module_name;

	/**
	* Clear logs flags - true if need to clear log files.
	*/
	bool m_clear_logs = false;

	/**
	* Use PID in message headers.
	*/
	bool m_use_pid_in_message_headers = false;

	/**
	* Use PID in log file name.
	*/
	bool m_use_pid_in_file_name = false;

	/**
	* Use one log file instead three: debug, info, error.
	*/
	bool m_use_log_file = false;

	/**
	* Use console output
	*/
	bool m_use_console_output = false;

	/**
	* Logger level.
	* Log entries of the same level or below will be written
	*/
	unsigned int m_logger_level = LoggerLevel::Debug;
};
}