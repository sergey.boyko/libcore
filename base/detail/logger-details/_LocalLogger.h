/**
* Created by Sergey O. Boyko on 18.06.18.
*/

#pragma once

#include <iostream>

#include "base/Logger.h"

#ifdef CORE_DEBUG

/**
* Log on. Used only by libcore to debugging
*/
#define LOG_C(format, args...) LOG_D(format, args)

#else

/**
* Empty macros
*/
#define LOG_C(format, args...)

#endif