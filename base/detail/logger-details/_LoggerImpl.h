/**
* Created by Sergey O. Boyko on 03.06.18.
*/

#pragma once

#include <string_view>
#include <mutex>
#include <optional>
#include <fstream>
#include <variant>
#include <iostream>

#include "base/ProcessId.h"
#include "base/Formatter.h"
#include "base/detail/TimeCommon.h"

#include "_LoggerOptions.h"

namespace core::base::detail {

/**
* Logger standalone implementation. Used to format informative log messages
* and to write them to standard output and to a file.
* Message format:
* [time pid]: file:line: pid: some message
*/
class _LoggerImpl: _LoggerOptions {
public:
	/**
	* Initialize logger - accept the entered options
	* @return true if _LoggerImpl instance is initialized correctly,
	* else false
	*/
	[[nodiscard]]
	static bool InitLogger();

	/**
	* Get last _LoggerImpl error text
	* @return - error text
	*/
	static std::string GetLastError();

	/**
	* Logger is initialized
	* @return - true if logger already is initialized, else false
	*/
	static bool IsInitialized();

	/**
	* Get logger standalone instance
	* @return
	*/
	static std::unique_ptr<_LoggerImpl> &GetInstance();

	/**
	* Use file to logging.
	* Default path is "/tmp/".
	* @param path - Path to log directory.
	*/
	static void UseLogFile(const std::experimental::filesystem::path &log_path);

	/**
	* Set the name of the module that is initializes the Logger.
	* Default module name is date.
	* @param name - Module name.
	*/
	static void SetModuleName(std::string_view name);

	/**
	* Logs will be cleared the next time write to a files.
	*/
	static void ClearLogs() noexcept;

	/**
	* Message header will include a PID.
	* Used for identify the process that logged the message to the log,
	* if all processes write to one log file ("UsePidInLogFileName()" was not enabled).
	*/
	static void UsePidInMessageHeader() noexcept;

	/**
	* Log file name will include PID.
	* Used for identify the process that logged to the log file.
	*/
	static void UsePidInLogFileName() noexcept;

	/**
	* Clear the entered options
	*/
	static void ClearOptions();

	/**
	* Use console output
	*/
	static void UseConsoleOutput() noexcept;

	/**
	* Set logger level.
	* Log entries of the same level or below will be written.
	* If you do not want logging then set the None level.
	* Default logger level = Debug
	* @param level: Error = 1, Info = 2, Debug = 3 or None = 0
	*/
	static void SetLevel(unsigned int level);

	/**
	* Virtual dtor
	*/
	virtual ~_LoggerImpl();

	/**
	* Create a log entry
	* @tparam Types - inline argument types
	* @param log_type - log entry type: Debug, Error, Info
	* @param src_file - file of the called function
	* @param line - line number of the called function
	* @param str - log message
	* @param args - inline arguments
	*/
	template <typename ...Types>
	void LogOn(unsigned int log_type,
						 std::string_view src_file,
						 unsigned int line,
						 const std::string &str,
						 Types &&... args) {
		// lock mutex to synchronize the logging
		auto lock = getMutexLock();

		// check logger level
		if(log_type > m_logger_level) {
			return;
		}

		auto current_time = GetCurrentTimeAsString(time(nullptr));
		auto message = Format(str, std::forward<Types>(args)...);
		auto message_header = current_time;
		auto str_log_type = getLogTypeAsString(log_type);

		if(m_use_pid_in_message_headers) {
			message_header = Format("% %",
															current_time,
															GetCurrentProcessId());
		}

		auto full_log_message = Format("[%]: %:%: %: %",
																	 message_header,
																	 src_file,
																	 line,
																	 str_log_type,
																	 message);

		if(m_use_console_output) {
			std::cout << full_log_message << std::endl << std::flush;
		}

		if(m_log_fstream) {
			*m_log_fstream << full_log_message << std::endl;
		}
	}

	/**
	* Insert a new line character in output sequence (without message header)
	*/
	void LogEndl();

protected:
	/**
	* Logger protected instance
	*/
	static std::unique_ptr<_LoggerImpl> _Instance;

	/**
	* Entered options.
	* Used to initialize a _LoggerImpl instance
	*/
	static _LoggerOptions _InitializationOptions;

	/**
	* Protected ctor
	* @param options - Logger options
	*/
	_LoggerImpl();

	/**
	* Get mutex lock guard.
	* @return std::lock_guard if application supports multithreading, else std::nullopt
	*/
	std::optional<std::lock_guard<std::mutex>> getMutexLock();

	/**
	* Open log file through fstream
	* @return - true if fstream is opened correctly, else false
	*/
	bool openFileStream();

	/**
	* Generate log file name
	* @return - file name
	*/
	std::string generateFileName();

	/**
	* Convert log_type as number to string
	* @param log_type - log entry type
	* @return - log entry type as string
	*/
	std::string getLogTypeAsString(unsigned int log_type);

	/**
	* Opened logger file stream.
	* If m_use_one_file is true, then m_file_stream have only one open file stream
	*/
	std::optional<std::ofstream> m_log_fstream;

	/**
	* Is initialization failed flag.
	* True if an error occurred during initialization, else false
	*/
	bool m_is_initialization_error = false;

	/**
	* Initialization text error
	*/
	std::string m_initialization_error;

#ifdef CORE_MULTITHREADED
	/**
	* Mutex used to protect access to a recording.
	*/
	std::mutex m_mutex;
#endif
};

} //end of namespace core::base
