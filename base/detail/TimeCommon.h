/**
* Created by Sergey O. Boyko on 18.06.18.
*/

#pragma once

#include <string>
#include <ctime>

namespace core::base::detail {

/**
* Get current date as string.
* String format: year-month-day
* @param current_time - current timestamp
* @return - current date as string
*/
std::string GetDateAsString(std::time_t current_time);

/**
* Get current time as string.
* String format: hour:min:sec:mill
* @param current_time - current timestamp
* @return - current time as string
*/
std::string GetCurrentTimeAsString(std::time_t current_time);

}