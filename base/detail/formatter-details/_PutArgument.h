//
// Created by bso on 23.02.19.
//

#pragma once

#include <string>
#include <cassert>

namespace core::base::detail {

/**
 * Put the argument to format string
 * @param last_processed_pos - position of the part which should be processed
 * @param format - format string
 * @param arg - input argument
 * @return - a position of the unprocessed part
 *           (position of the part which should be processed on next iteration)
 */
inline std::size_t _PutArgument(std::size_t last_processed_pos, std::string &format, std::string &&arg) {
  std::size_t in_position = format.find_first_of('%', last_processed_pos);
  // check if there is '%'
  assert(in_position != std::string::npos
         && "The number of arguments is more than necessary");

  format.erase(in_position, 1);
  // TODO use std::move(arg) if there is insert(T &&arg) implementation
  format.insert(in_position, arg);
  return in_position + arg.size();
}

}
