//
// Created by bso on 23.02.19.
//

#pragma once

#include <utility>

#include "base/ToString.h"
#include "base/detail/formatter-details/_PutArgument.h"

namespace core::base::detail {

/**
 * Process argument: put the arg (as string) to format string.
 * In this case we have the last argument
 * @tparam T - argument type [*** must not be a pointer***] else there will compile error
 * @param last_processed_pos - position of the part which should be processed
 * @param format - format string
 * @param arg - last input argument
 */
template <typename T>
void _ProcessArguments(std::size_t last_processed_pos, std::string &format, T &&arg) {
  last_processed_pos = _PutArgument(
      last_processed_pos,
      format,
      ToString(std::forward<T>(arg))
  );

  assert(format.find_first_of('%', last_processed_pos) == std::string::npos
         && "The number of arguments is less than necessary");
}

/**
 * Process arguments: put the arg (as string) to format string
 * and call itself to separate the first element from the remaining arguments and continue processing
 * @tparam T - separated argument type
 * @tparam Types - types of the remaining arguments
 * @param last_processed_pos - position of the part which should be processed
 * @param format - format string
 * @param arg - separated argument
 * @param args - remaining arguments
 */
template <typename T, typename ... Types>
void _ProcessArguments(std::size_t last_processed_pos, std::string &format, T &&arg, Types &&... args) {
  // put the separated argument to format string
  last_processed_pos = _PutArgument(
      last_processed_pos,
      format,
      ToString(std::forward<T>(arg))
  );

  // continue processing
  _ProcessArguments(last_processed_pos, format, std::forward<Types>(args)...);
}

/**
 * Process arguments: call the _ProcessArguments (another signature) to separate the first element from the arguments
 * @tparam Types - Types of the input arguments
 * @param format - format string
 * @param args - input arguments
 */
template <typename ... Types>
void _ProcessArguments(std::string &format, Types &&... args) {
  _ProcessArguments(0, format, std::forward<Types>(args)...);
}

} // end of core::base::detail
