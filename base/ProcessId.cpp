/**
* Created by Sergey O. Boyko on 08.07.18.
*/

#include "ProcessId.h"

#if defined(__linux__) || defined(__APPLE__)

#include <zconf.h>

namespace core::base {
unsigned int GetCurrentProcessId() {
	return static_cast<unsigned int>(getpid());
}
}

#elif defined(_WIN32) || defined(_WIN64)

#include <Windows.h>

namespace core::base {
unsigned int GetCurrentProcessId() {
	return static_cast<unsigned int>(GetCurrentProcessId());
}
}

#else
namespace core::base {
unsigned int GetCurrentProcessId() {
	std::throw std::runtime_error("GetCurrentProcessId() called on an unknown OS");
}
}
#endif