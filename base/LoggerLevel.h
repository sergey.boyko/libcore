//
// Created by bso on 12.08.18.
//

#pragma once

namespace core::base::LoggerLevel {
enum T {
	None = 0,
	Error = 1,
	Info = 2,
	Debug = 3,
};
}
