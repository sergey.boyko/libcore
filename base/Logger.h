/**
* Created by Sergey O. Boyko on 22.06.18.
*/

#pragma once

#include "detail/logger-details/_LoggerImpl.h"
#include "LoggerLevel.h"

typedef core::base::detail::_LoggerImpl Logger;

/**
* Create a debug log entry
*/
#define LOG_D(format, ...) Logger::GetInstance()->LogOn(\
core::base::LoggerLevel::Debug,\
__FILE__,\
__LINE__,\
format,\
##__VA_ARGS__\
)

/**
* Create an info log entry
*/
#define LOG_I(format, ...) Logger::GetInstance()->LogOn(\
core::base::LoggerLevel::Info,\
__FILE__,\
__LINE__,\
format,\
##__VA_ARGS__\
)

/**
* Create an error log entry
*/
#define LOG_E(format, ...) Logger::GetInstance()->LogOn(\
core::base::LoggerLevel::Error,\
__FILE__,\
__LINE__,\
format,\
##__VA_ARGS__\
)

#define LOG_ENDL() Logger::GetInstance()->LogEndl()