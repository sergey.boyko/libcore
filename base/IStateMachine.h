/**
* Created by Sergey O. Boyko on 10.06.18.
*/

#pragma once

#include <memory>
#include "detail/logger-details/_LocalLogger.h"

namespace core::base {

template <typename CTX>
class IStateContext;

/**
* State implementation - universal interface.
* IState is used to encapsulation of the behavior of the context in this state.
* @tparam CTX - state context
*/
template <class CTX>
struct IState {
	/**
	* Ctor
	* @param state_id - state identifier
	*/
	explicit IState(unsigned int state_id):
			m_state_id(state_id) {
	}

	/**
	* State entry handler.
	* Called when this state occurs
	*/
	virtual void OnChanged() = 0;

	/**
	* Check next state
	* @param state_id - state identifier
	* @return - true if the next state is allowed, else false
	*/
	virtual bool CheckNextStateAllowed(unsigned int next_state_id) = 0;

	/**
	* State context pointer
	*/
	std::shared_ptr<CTX> m_ctx;

	unsigned int m_state_id;
};

template <class CTX>
class IStateContext: public std::enable_shared_from_this<CTX> {
protected:
	/**
	* Handler is called if an disallowed state is set
	*/
	virtual void onDisallowedState() = 0;

	/**
	* Set state.
	* If state is disallowed then will be called "onDisallowedState"
	* @tparam S - state type
	* @tparam Types - argument types
	* @param args - arguments
	*/
	template <class S, class ...Types>
	void setState(Types &&... args) {
		auto next_state = new S(std::forward<Types>(args)...);

		if(checkNextStateAllowed(next_state)) {
			next_state->m_ctx = this->shared_from_this();
			m_state.reset(next_state);
			m_state->OnChanged();
		} else {
			onDisallowedState();
		}
	}

	/**
	* Get state name as string from state_id
	* @param state_id - state id
	* @return - state name as string
	*/
	virtual std::string getStateName(unsigned int state_id) = 0;

	/**
	* State pointer
	*/
	std::unique_ptr<IState<CTX>> m_state;

private:
	/**
	* Check next state
	* @tparam S - next state type
	* @param next_state - next state pointer
	* @return true if the next state is allowed, else false
	*/
	template <class S>
	bool checkNextStateAllowed(S *next_state) {
		if(!m_state) {
			LOG_C("set state from NULL to %", getStateName(next_state->m_state_id));
			return true;
		}

		if(!m_state->CheckNextStateAllowed(next_state->m_state_id)) {
			LOG_C("next state % is not allowed", getStateName(next_state->m_state_id));
			return false;
		}

		LOG_C("change state from % to %",
					getStateName(m_state->m_state_id),
					getStateName(next_state->m_state_id));
		return true;
	}
};

}
