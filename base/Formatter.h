/**
* Created by Sergey O. Boyko on 11.07.18.
*/

#pragma once

#include "detail/formatter-details/_ProcessArguments.h"

namespace core::base {

/**
* Puts input arguments into the template string
* @tparam Types - input argument types
* @param format - template string
* @param args - input arguments
* @return - result formatted string
*/
template<typename ... Types>
std::string Format(std::string format, Types &&... args) {
	detail::_ProcessArguments(format, std::forward<Types>(args)...);
	return format;
}

/**
* The function used when there are no input arguments
* @param format - format string
* @return - the format string
*/
inline std::string Format(const std::string &format) {
	return format;
}

}
