/**
* Created by Sergey O. Boyko on 11.06.18.
*/

#pragma once

#include "Logger.h"

namespace core::base {

/**
* Check condition. If condition is false then an exception will be thrown
*/
#define CHECK(condition, message, ...) if(!(condition)) {\
  std::string __full_message__ = core::base::Format((message), ##__VA_ARGS__);\
  if(Logger::IsInitialized()) {\
    LOG_E(__full_message__);\
  }\
  throw std::runtime_error(__full_message__);\
}

#ifdef CORE_DEBUG

/**
* Check condition. If condition is false
* then will create a error log entry and an exception will be thrown
*/
#define CHECK_D(condition, message, ...) CHECK(condition, message, ##__VA_ARGS__)

/**
* Create a error log entry and throw exception
*/
#define THROWEX_D(message, ...) CHECK(false, message, ##__VA_ARGS__)

#else

/**
* Check condition. If condition is false
* then will create a error log entry
*/
#define CHECK_D(condition, message, ...) if(!(condition)) {\
  if(Logger::IsInitialized()) {\
    LOG_E((message), ##__VA_ARGS__);\
  }\
}

/**
* Create a error log entry
*/
#define THROWEX_D(message) CHECK_D(true, message)

#endif

}
