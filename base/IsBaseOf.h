//
// Created by bso on 21.07.18.
//

#pragma once

#include <type_traits>
#include <stdexcept>

#include "Checking.h"

#define ARG_IS_BASE 0
#define ARG_IS_DERIVED 1

/**
* Checking that BASE_TYPE is base of DERIVED_TYPE
* Arguments must be rvalue. Example of use:
* if( IsBaseOf(std::move(base_exemplar), std::move(derived_exemplar)) ) {...}
* @tparam BASE_TYPE - base class instance rvalue type
* @tparam DERIVED_TYPE - derived class instance rvalue type
* @return - true if BASE_TYPE is base of DERIVED_TYPE
*/
template <typename BASE_TYPE, typename DERIVED_TYPE>
bool IsBaseOf(BASE_TYPE &&, DERIVED_TYPE &&) {
	return std::is_base_of<BASE_TYPE, DERIVED_TYPE>();
};

/**
* Checking that T is base of T_ARG if ARG_INDEX == ARG_IS_DERIVED, or
* checking that T_ARG is base of T if ARG_INDEX == ARG_IS_BASE.
* Argument must be rvalue. Example of use:
* if( IsBaseOf<BASE_TYPE, ARG_IS_DERIVED>( std::move(derived_exemplar)) ) {...}
* @tparam T - first type
* @tparam ARG_TYPE_INDEX - argument type index
* @tparam T_ARG - rvalue argument type
* @return - true or false
*/
template <typename T, unsigned int ARG_TYPE_INDEX, typename T_ARG>
bool IsBaseOf(T_ARG &&) {
	switch(ARG_TYPE_INDEX) {
		case ARG_IS_BASE: {
			return std::is_base_of<T_ARG, T>();
		}

		case ARG_IS_DERIVED: {
			return std::is_base_of<T, T_ARG>();
		}

		default: {
			THROWEX_D(R"(ARG_TYPE_INDEX must be 'ARG_IS_BASE' or 'ARG_IS_DERIVED')");
		}
	}
};