//
// Created by bso on 22.07.18.
//

#pragma once

#include <type_traits>

/**
* Checking that T1 is same as T2.
* Arguments must be rvalue. Example of use:
* if( IsSameType<SOME_TYPE>(std::move(arg)) ) {...}
* @tparam T1 - verifiable type
* @tparam T2 - argument rvalue type
* @return - true if T1 is same as T2, else false
*/
template <typename T1, typename T2>
bool IsSameType(T2 &&) {
	return std::is_same<T1, T2>();
};

/**
* Checking that T1 is same as T2.
* Arguments must be rvalue. Example of use:
* if( IsSameType(std::move(arg1), std::move(arg2)) ) {...}
* @tparam T1 - first argument rvalue type
* @tparam T2 - second argument rvalue type
* @return - true if T1 is same as T2, else false
*/
template <typename T1, typename T2>
bool IsSameType(T1 &&, T2 &&) {
	return std::is_same<T1, T2>();
};