//
// Created by bso on 18.07.18.
//

/**
* Convert argument to string.
* If possible, the 'std::to_string' is used,
* because it is faster than std::stringstream
*/

#pragma once

#include <string>
#include <sstream>
#include <experimental/filesystem>

#ifdef CORE_PROTOBUF

#include <google/protobuf/util/json_util.h>

namespace core::base::detail {
/**
* Convert google::protobuf::Message arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(const google::protobuf::Message &arg) {
  std::string json_output;
  google::protobuf::util::MessageToJsonString(arg, &json_output);
  return json_output;
}
}

#endif

#ifdef CORE_JSONCPP

#include "third-party/jsoncpp/json.h"

namespace core::base::detail {
/**
* Convert Json::Value arg ptr to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(const Json::Value *arg) {
  Json::StreamWriterBuilder builder;
  return Json::writeString(builder, *arg);
}
}

#endif

#ifdef CORE_NET

#include <net/TcpPacket.h>

namespace core::base::detail {

/**
* Convert TcpPacketHeader to string
* format: "type = %, id = %, size = %"
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(const TcpPacketHeader &arg) {
  // do not use Formatter, because ToString.h included in _GetArgumentAsString
  std::stringstream ss;
  ss << "type = " << arg.m_type <<
     ", id = " << arg.m_id <<
     ", size = " << arg.m_size;
  return ss.str();
}
}
#endif

namespace core::base::detail {

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(char arg) {
  return {arg};
}

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(int arg) {
  return std::to_string(arg);
}

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(unsigned int arg) {
  return std::to_string(arg);
}

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(long arg) {
  return std::to_string(arg);
}

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(unsigned long arg) {
  return std::to_string(arg);
}

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(long long arg) {
  return std::to_string(arg);
}

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(unsigned long long arg) {
  return std::to_string(arg);
}

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(float arg) {
  return std::to_string(arg);
}

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(double arg) {
  return std::to_string(arg);
}

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(long double arg) {
  return std::to_string(arg);
}

/**
* Convert arg to string
* @param arg - convertible argument
* @return - arg as string
*/
inline std::string ToString(std::string_view arg) {
  return std::string(arg);
}

}
