//
// Created by bso on 04.08.18.
//

#ifndef LIBCORE_TEST_CONSTANTS_H
#define LIBCORE_TEST_CONSTANTS_H

namespace core::net::detail {

enum T {
	MAXIMUM_PACKET_SIZE = 1024,
	SOCKET_BUFFER_SIZE = 4096
};

}

#endif //LIBCORE_TEST_CONSTANTS_H
