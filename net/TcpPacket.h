//
// Created by bso on 06.08.18.
//

#pragma once

#include <string>

#if defined(__linux__) || defined(__APPLE__)
#include <netinet/in.h>
#elif defined(_WIN32) || defined(_WIN64)
#include <winsock2.h>
#endif

#pragma pack(push, 1)

/**
* TCP packet header - main packet info
*/
struct TcpPacketHeader {
	/**
	* Packet type: Json, Protobuf etc
	*/
	uint16_t m_type;

	/**
	* Packet id
	*/
	uint16_t m_id;

	/**
	* Data size (without TcpPacketHeader)
	*/
	uint32_t m_size;
};

/**
* TCP packet
*/
struct TcpPacket {
	/**
	* Packet header (info)
	*/
	TcpPacketHeader m_info;

	/**
	* Data
	*/
	std::string m_data;
};

#pragma pack(pop)

inline void ConvertToNetworkByteOrder(TcpPacketHeader &header) {
	header.m_type = htons(header.m_type);
	header.m_id = htons(header.m_id);
	header.m_size = htonl(header.m_size);
}

inline void ConvertToHostByteOrder(TcpPacketHeader &header) {
	header.m_type = ntohs(header.m_type);
	header.m_id = ntohs(header.m_id);
	header.m_size = ntohl(header.m_size);
}