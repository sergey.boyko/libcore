//
// Created by bso on 04.08.18.
//

#pragma once

#include <vector>
#include <optional>

#include "TcpPacket.h"
#include "detail/Constants.h"

namespace core::net {

class TcpTokenizer {
public:
	TcpTokenizer() = default;

	/**
	* Process data. Input data will be cut into separate packets,
	* if possible (at least one TCP packet is completed)
	* @return - vector of separate TCP packets
	*/
	std::vector<TcpPacket> ProcessData(const char *data, std::size_t size);

	/**
	* Make output packet data
	* @param packet_id - packet id
	* @param packet_type - packet type: Json, Protobuf etc
	* @param packet_message - packet message
	* @return - output data
	*/
	std::string MakeOutputData(uint16_t packet_type,
														 uint16_t packet_id,
														 std::string_view packet_message) const;

	/**
	* Get m_buffer size
	* @return m_buffer size
	*/
	inline std::size_t GetBufferSize() const noexcept {
		return m_buffer.size();
	}

	/**
	* Clear buffer
	*/
	inline void ClearBuffer() noexcept {
		m_buffer.clear();
	}

protected:
	/**
	* Try to process one packet from the buffer (m_buffer)
	* @return - if managed to process one packet then return TcpPacket
	* else return nothing
	*/
	std::optional<TcpPacket> processOnePacket();

	/**
	* Input buffer
	*/
	std::string m_buffer;
};

}


