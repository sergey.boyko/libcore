//
// Created by bso on 04.08.18.
//

#include <cstring>

#include "TcpTokenizer.h"

std::vector<TcpPacket> core::net::TcpTokenizer::ProcessData(const char *data, std::size_t size) {
	std::vector<TcpPacket> packets;

	// push received data to buffer
	m_buffer.append(data, data + size);

	while(!m_buffer.empty()) {
		auto packet = processOnePacket();
		if(packet) {
			packets.push_back(*packet);
		} else {
			// break instead return because the operator "return" is heavy
			break;
		}
	}

	return packets;
}


std::string core::net::TcpTokenizer::MakeOutputData(uint16_t packet_type,
																								 uint16_t packet_id,
																								 std::string_view packet_message) const {
	TcpPacketHeader header {packet_type,
													packet_id,
													static_cast<uint32_t>(packet_message.size())};
	ConvertToNetworkByteOrder(header);

	std::string output_data(reinterpret_cast<char *>(&header), sizeof(TcpPacketHeader));
	output_data.append(packet_message);

	return output_data;
}


std::optional<TcpPacket> core::net::TcpTokenizer::processOnePacket() {
	auto header_size = sizeof(TcpPacketHeader);
	if(m_buffer.size() < header_size) {
		// received incomplete packet
		return {};
	}

	TcpPacket packet {};
	memcpy(&packet.m_info, m_buffer.data(), header_size);
	ConvertToHostByteOrder(packet.m_info);

	if((packet.m_info.m_size > m_buffer.size() - header_size)) {
		// received incomplete packet
		return {};
	}

	// set packet data
	packet.m_data.append(m_buffer.begin() + header_size,
											 m_buffer.begin() + header_size + packet.m_info.m_size);
	// erase processed packet from the top of the m_buffer
	m_buffer.erase(m_buffer.begin(), m_buffer.begin() + header_size + packet.m_info.m_size);

	return packet;
}
